$('document').ready(function () {
    var arrayDetachment = {
        "patrol": {
            "id": "patrol",
            "name": "Patrol",
            "command": 0,
            "elite": {
                "min": 0,
                "max": 2,
            },
            "dedicated_transport": {
                "min": 0,
                "max": 13,
            },
            "fast_attack": {
                "min": 0,
                "max": 2,
            },
            "flyer": {
                "min": 0,
                "max": 2,
            },
            "fortification": {
                "min": 0,
                "max": 0,
            },
            "heavy_support": {
                "min": 0,
                "max": 2,
            },
            "hq": {
                "min": 1,
                "max": 2,
            },
            "lord_of_war": {
                "min": 0,
                "max": 0,
            },
            "troop": {
                "min": 1,
                "max": 3,
            }
        },
        "battalion": {
            "id": "battalion",
            "name": "Battalion",
            "command": 3,
            "elite": {
                "min": 0,
                "max": 6,
            },
            "dedicated_transport": {
                "min": 0,
                "max": 23,
            },
            "fast_attack": {
                "min": 0,
                "max": 3,
            },
            "flyer": {
                "min": 0,
                "max": 2,
            },
            "fortification": {
                "min": 0,
                "max": 0,
            },
            "heavy_support": {
                "min": 0,
                "max": 3,
            },
            "hq": {
                "min": 2,
                "max": 3,
            },
            "lord_of_war": {
                "min": 0,
                "max": 0,
            },
            "troop": {
                "min": 3,
                "max": 6,
            }
        },
        "brigade": {
            "id": "brigade",
            "name": "Brigade",
            "command": 3,
            "elite": {
                "min": 3,
                "max": 8,
            },
            "dedicated_transport": {
                "min": 0,
                "max": 37,
            },
            "fast_attack": {
                "min": 3,
                "max": 5,
            },
            "flyer": {
                "min": 0,
                "max": 2,
            },
            "fortification": {
                "min": 0,
                "max": 0,
            },
            "heavy_support": {
                "min": 3,
                "max": 5,
            },
            "hq": {
                "min": 3,
                "max": 5,
            },
            "lord_of_war": {
                "min": 0,
                "max": 0,
            },
            "troop": {
                "min": 6,
                "max": 12,
            }
        },
        "vanguard": {
            "id": "vanguard",
            "name": "Vanguard",
            "command": 1,
            "elite": {
                "min": 3,
                "max": 6,
            },
            "dedicated_transport": {
                "min": 0,
                "max": 17,
            },
            "fast_attack": {
                "min": 0,
                "max": 2,
            },
            "flyer": {
                "min": 0,
                "max": 2,
            },
            "fortification": {
                "min": 0,
                "max": 0,
            },
            "heavy_support": {
                "min": 0,
                "max": 2,
            },
            "hq": {
                "min": 1,
                "max": 2,
            },
            "lord_of_war": {
                "min": 0,
                "max": 0,
            },
            "troop": {
                "min": 0,
                "max": 3,
            }
        },
        "spearhead": {
            "id": "spearhead",
            "name": "Spearhead",
            "command": 1,
            "elite": {
                "min": 0,
                "max": 2,
            },
            "dedicated_transport": {
                "min": 0,
                "max": 17,
            },
            "fast_attack": {
                "min": 0,
                "max": 2,
            },
            "flyer": {
                "min": 0,
                "max": 2,
            },
            "fortification": {
                "min": 0,
                "max": 0,
            },
            "heavy_support": {
                "min": 3,
                "max": 6,
            },
            "hq": {
                "min": 1,
                "max": 2,
            },
            "lord_of_war": {
                "min": 0,
                "max": 0,
            },
            "troop": {
                "min": 0,
                "max": 3,
            }
        },
        "outrider": {
            "id": "outrider",
            "name": "Outrider",
            "command": 1,
            "elite": {
                "min": 0,
                "max": 2,
            },
            "dedicated_transport": {
                "min": 0,
                "max": 17,
            },
            "fast_attack": {
                "min": 3,
                "max": 6,
            },
            "flyer": {
                "min": 0,
                "max": 2,
            },
            "fortification": {
                "min": 0,
                "max": 0,
            },
            "heavy_support": {
                "min": 0,
                "max": 2,
            },
            "hq": {
                "min": 1,
                "max": 2,
            },
            "lord_of_war": {
                "min": 0,
                "max": 0,
            },
            "troop": {
                "min": 0,
                "max": 3,
            }
        },
        "supreme_command": {
            "id": "supreme_command",
            "name": "Supreme Command",
            "command": 1,
            "elite": {
                "min": 0,
                "max": 1,
            },
            "dedicated_transport": {
                "min": 0,
                "max": 7,
            },
            "fast_attack": {
                "min": 0,
                "max": 0,
            },
            "flyer": {
                "min": 0,
                "max": 0,
            },
            "fortification": {
                "min": 0,
                "max": 0,
            },
            "heavy_support": {
                "min": 0,
                "max": 0,
            },
            "hq": {
                "min": 3,
                "max": 5,
            },
            "lord_of_war": {
                "min": 0,
                "max": 1,
            },
            "troop": {
                "min": 0,
                "max": 0,
            }
        },
        "supreme_heavy": {
            "id": "supreme_heavy",
            "name": "Supreme Heavy",
            "command": 3,
            "elite": {
                "min": 0,
                "max": 0,
            },
            "dedicated_transport": {
                "min": 0,
                "max": 0,
            },
            "fast_attack": {
                "min": 0,
                "max": 0,
            },
            "flyer": {
                "min": 0,
                "max": 0,
            },
            "fortification": {
                "min": 0,
                "max": 0,
            },
            "heavy_support": {
                "min": 0,
                "max": 0,
            },
            "hq": {
                "min": 0,
                "max": 0,
            },
            "lord_of_war": {
                "min": 3,
                "max": 5,
            },
            "troop": {
                "min": 0,
                "max": 0,
            }
        },
        "air_wing": {
            "id": "air_wing",
            "name": "Air Wing",
            "command": 1,
            "elite": {
                "min": 0,
                "max": 0,
            },
            "dedicated_transport": {
                "min": 0,
                "max": 0,
            },
            "fast_attack": {
                "min": 0,
                "max": 0,
            },
            "flyer": {
                "min": 3,
                "max": 5,
            },
            "fortification": {
                "min": 0,
                "max": 0,
            },
            "heavy_support": {
                "min": 0,
                "max": 0,
            },
            "hq": {
                "min": 0,
                "max": 0,
            },
            "lord_of_war": {
                "min": 0,
                "max": 0,
            },
            "troop": {
                "min": 0,
                "max": 0,
            }
        },
        "super_heavy_auxiliary": {
            "id": "super_heavy_auxiliary",
            "name": "Super Heavy Auxiliary",
            "command": 0,
            "elite": {
                "min": 0,
                "max": 0,
            },
            "dedicated_transport": {
                "min": 0,
                "max": 0,
            },
            "fast_attack": {
                "min": 0,
                "max": 0,
            },
            "flyer": {
                "min": 0,
                "max": 0,
            },
            "fortification": {
                "min": 0,
                "max": 0,
            },
            "heavy_support": {
                "min": 0,
                "max": 0,
            },
            "hq": {
                "min": 0,
                "max": 0,
            },
            "lord_of_war": {
                "min": 0,
                "max": 1,
            },
            "troop": {
                "min": 0,
                "max": 0,
            }
        },
        "fortification_network": {
            "id": "fortification_network",
            "name": "Fortification Network",
            "command": 0,
            "elite": {
                "min": 0,
                "max": 0,
            },
            "dedicated_transport": {
                "min": 0,
                "max": 0,
            },
            "fast_attack": {
                "min": 0,
                "max": 0,
            },
            "flyer": {
                "min": 0,
                "max": 0,
            },
            "fortification": {
                "min": 1,
                "max": 3,
            },
            "heavy_support": {
                "min": 0,
                "max": 0,
            },
            "hq": {
                "min": 0,
                "max": 0,
            },
            "lord_of_war": {
                "min": 0,
                "max": 0,
            },
            "troop": {
                "min": 0,
                "max": 0,
            }
        },
        "auxiliary_support": {
            "id": "auxiliary_support",
            "name": "Auxiliary Support",
            "command": -1,
            "elite": {
                "min": 1,
                "max": 1,
            },
            "dedicated_transport": {
                "min": 1,
                "max": 1,
            },
            "fast_attack": {
                "min": 1,
                "max": 1,
            },
            "flyer": {
                "min": 1,
                "max": 1,
            },
            "fortification": {
                "min": 0,
                "max": 0,
            },
            "heavy_support": {
                "min": 1,
                "max": 1,
            },
            "hq": {
                "min": 1,
                "max": 1,
            },
            "lord_of_war": {
                "min": 0,
                "max": 0,
            },
            "troop": {
                "min": 1,
                "max": 1,
            }
        }
    }
    $("#generate_detachment").on('click', function () {
        var unitCount = {
            "elite": localStorage.getItem('elite'),
            "dedicated_transport": localStorage.getItem('dedicated_transport'),
            "fast_attack": localStorage.getItem('fast_attack'),
            "flyer": localStorage.getItem('flyer'),
            "fortification": localStorage.getItem('fortification'),
            "heavy_support": localStorage.getItem('heavy_support'),
            "hq": localStorage.getItem('hq'),
            "lord_of_war": localStorage.getItem('lord_of_war'),
            "troop": localStorage.getItem('troop'),
        }

        //  set a temp object to subtract counts from
        //  loop detachment
        //  if the unit count fits, create an array of detachments, and subtract the unit count from the temp object
        //  tally up the command points
        //  if there are left over, recursive with leftovers,

        //  list all detachments with command points and unit totals ( allow user to increase base unit count )
        //  allow user to select which detachment, reload detachments with new count
        //  then select additional detachments

        var tempUnitCount = unitCount;

        findDetachments(arrayDetachment, tempUnitCount);
    });
});

function findDetachments(arrayDetachment, unitCount) {
    var arrayAvailableDetachments = []; //  declare the array we are to push the detachment id to
    //  loop trhough each detachment type and see if the army list fits
    $.each(arrayDetachment, function (i, val) {
        if (val.elite['min'] <= unitCount['elite'] && val.elite['max'] >= unitCount['elite']
            && val.dedicated_transport['min'] <= unitCount['dedicated_transport'] && val.dedicated_transport['max'] >= unitCount['dedicated_transport']
            && val.fast_attack['min'] <= unitCount['fast_attack'] && val.fast_attack['max'] >= unitCount['fast_attack']
            && val.flyer['min'] <= unitCount['flyer'] && val.flyer['max'] >= unitCount['flyer']
            && val.fortification['min'] <= unitCount['fortification'] && val.fortification['max'] >= unitCount['fortification']
            && val.heavy_support['min'] <= unitCount['heavy_support'] && val.heavy_support['max'] >= unitCount['heavy_support']
            && val.hq['min'] <= unitCount['hq'] && val.hq['max'] >= unitCount['hq']
            && val.lord_of_war['min'] <= unitCount['lord_of_war'] && val.lord_of_war['max'] >= unitCount['lord_of_war']
            && val.troop['min'] <= unitCount['troop'] && val.troop['max'] >= unitCount['troop']
        ) {
            arrayAvailableDetachments.push(val.id); //  push the id onto the array
            //  localStorage.setItem('elite', 0);
            //  localStorage.getItem('troop')
            //  localStorage[strType]--;
        }
    });

    $("#detachment_container").empty();
    $("#detachment_container").append("<ul id='detachment_list'></ul>");

    $.each(arrayAvailableDetachments, function (i, val) {
        detachment = "<li>" + arrayDetachment[val].name + "</li>";
        $("#detachment_list").append(detachment);
    });

    // console.dir(arrayAvailableDetachments);
}