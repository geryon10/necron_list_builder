$('document').ready(function () {
    var arrayUnit = {
        "annihilation_barge": {
            "id": "annihilation_barge",
            "name": "Annihilation Barge",
            "points": 133,
            "power_points": 7,
            "pp_per_unit": 7,
            "base_weapon_cost": 20,
            "type": "Heavy Supprt",
            "type_id": "heavy_support",
            "weapon": {
                "gauss_canon": {
                    "name": "Gauss Cannon",
                    "cost": 20,
                    "uniform": true,
                    "add_on": false,
                    "always_included": false,
                },
                "tesla_cannon": {
                    "name": "Tesla Cannon",
                    "cost": 13,
                    "uniform": true,
                    "add_on": false,
                    "always_included": false,
                },
                "twin_tesla_destructor": {
                    "name": "Twin Tesla Destructor",
                    "cost": 0,
                    "uniform": true,
                    "add_on": false,
                    "always_included": true,
                }
            },
            "min": 1,
            "max": 1,
        },
        "canoptek_acanthrites": {
            "id": "canoptek_acanthrites",
            "name": "Canoptek Acanthrites",
            "points": 54,
            "power_points": 9,
            "pp_per_unit": 3,
            "base_weapon_cost": 0,
            "type": "Fast Attack",
            "type_id": "fast_attack",
            "weapon": {
                "cutting_beam": {
                    "name": "Cutting Beam",
                    "cost": 0,
                    "uniform": true,
                    "add_on": false,
                    "always_included": true,
                },
                "voideblade": {
                    "name": "Voideblade",
                    "cost": 6,
                    "uniform": true,
                    "add_on": false,
                    "always_included": true,
                },
            },
            "min": 3,
            "max": 9,
        },
        "canoptek_scarab": {
            "id": "canoptek_scarab",
            "name": "Canoptek Scarab",
            "points": 13,
            "power_points": 2,
            "pp_per_unit": (2 / 3),
            "base_weapon_cost": 0,
            "type": "Fast Attack",
            "type_id": "fast_attack",
            "weapon": {
                "feeder_mandibles": {
                    "name": "Feeder Mandibles",
                    "cost": 0,
                    "uniform": true,
                    "add_on": false,
                    "always_included": true,
                },
            },
            "min": 3,
            "max": 9,
        },
        "canoptek_spyder": {
            "id": "canoptek_spyder",
            "name": "Canoptek Spyder",
            "points": 76,
            "power_points": 4,
            "pp_per_unit": 4,
            "base_weapon_cost": 0,
            "type": "Heavy Supprt",
            "type_id": "heavy_support",
            "weapon": {
                "automaton_claws": {
                    "name": "Automaton Claws",
                    "cost": 0,
                    "uniform": true,
                    "add_on": false,
                    "always_included": true,
                },
                "particle_beamer": {
                    "name": "2 Particle Beamer",
                    "cost": 20,
                    "uniform": false,
                    "add_on": true,
                    "always_included": false,
                },
            },
            "additional_gear": {
                "gloom_prism": {
                    "name": "Gloom Prism",
                    "cost": 5,
                    "uniform": false,
                    "add_on": true,
                    "always_included": false,
                },
                "fabricator_claw_array": {
                    "name": "Fabricator Claw Array",
                    "cost": 8,
                    "uniform": false,
                    "add_on": true,
                    "always_included": false,
                },
            },
            "min": 1,
            "max": 3,
        },
        "canoptek_tomb_sentinel": {
            "id": "canoptek_tomb_sentinel",
            "name": "Canoptek Tomb Sentinel",
            "points": 180,
            "power_points": 9,
            "pp_per_unit": 9,
            "base_weapon_cost": 20,
            "type": "Fast Attack",
            "type_id": "fast_attack",
            "weapon": {
                "automaton_claws": {
                    "name": "Automaton Claws",
                    "cost": 0,
                    "uniform": true,
                    "add_on": false,
                    "always_included": true,
                },
                "exile_cannon": {
                    "name": "Exile Cannon",
                    "cost": 20,
                    "uniform": true,
                    "add_on": false,
                    "always_included": true,
                },
            },
            "min": 1,
            "max": 1,
        },
        "canoptek_tomb_stalker": {
            "id": "canoptek_tomb_stalker",
            "name": "Canoptek Tomb Stalker",
            "points": 180,
            "power_points": 8,
            "pp_per_unit": 8,
            "base_weapon_cost": 0,
            "type": "Elite",
            "type_id": "elite",
            "weapon": {
                "automaton_claws": {
                    "name": "Automaton Claws",
                    "cost": 0,
                    "uniform": true,
                    "add_on": false,
                    "always_included": true,
                },
                "twin_gauss_slicers": {
                    "name": "Twin Gauss Slicers",
                    "cost": 0,
                    "uniform": true,
                    "add_on": false,
                    "always_included": true,
                },
            },
            "additional_gear": {
                "gloom_prism": {
                    "name": "Gloom Prism",
                    "cost": 5,
                    "uniform": false,
                    "add_on": true,
                    "always_included": false,
                },
            },
            "min": 1,
            "max": 1,
        },
        "canoptek_wraith": {
            "id": "canoptek_wraith",
            "name": "Canoptek Wraith",
            "points": 38,
            "power_points": 6,
            "pp_per_unit": 2,
            "base_weapon_cost": 0,
            "type": "Fast Attack",
            "type_id": "fast_attack",
            "weapon": {
                "vicious_claws": {
                    "name": "Vicious Claws",
                    "cost": 0,
                    "uniform": true,
                    "add_on": false,
                    "always_included": true,
                },
                "particle_caster": {
                    "name": "Particle Caster",
                    "cost": 4,
                    "uniform": false,
                    "add_on": true,
                    "always_included": false,
                },
                "transdimensional_beamer": {
                    "name": "Transdimensional Beamer",
                    "cost": 14,
                    "uniform": false,
                    "add_on": true,
                    "always_included": false,
                },
                "whip_coils": {
                    "name": "Whip Coils",
                    "cost": 9,
                    "uniform": false,
                    "add_on": true,
                    "always_included": false,
                },
            },
            "min": 3,
            "max": 6,
        },
        "catacomb_command_barge": {
            "id": "catacomb_command_barge",
            "name": "Catacomb Command Barge",
            "points": 138,
            "power_points": 10,
            "pp_per_unit": 10,
            "base_weapon_cost": 20,
            "type": "HQ",
            "type_id": "hq",
            "weapon": {
                "gauss_cannon": {
                    "name": "Gauss Cannon",
                    "cost": 20,
                    "uniform": true,
                    "add_on": false,
                    "always_included": false,
                },
                "tesla_cannon": {
                    "name": "Tesla Cannon",
                    "cost": 13,
                    "uniform": true,
                    "add_on": false,
                    "always_included": false,
                },
            },
            "additional_gear": {
                "hyperphase_sword": {
                    "name": "Hyperphase Sword",
                    "cost": 3,
                    "uniform": true,
                    "add_on": false,
                    "always_included": false,
                },
                "voideblade": {
                    "name": "Voideblade",
                    "cost": 6,
                    "uniform": true,
                    "add_on": false,
                    "always_included": false,
                },
                "warscythe": {
                    "name": "Warscythe",
                    "cost": 11,
                    "uniform": true,
                    "add_on": false,
                    "always_included": false,
                },
            },
            "min": 1,
            "max": 1,
        },
        "cryptek": {
            "id": "cryptek",
            "name": "Cryptek",
            "points": 86,
            "power_points": 6,
            "pp_per_unit": 6,
            "base_weapon_cost": 18,
            "type": "HQ",
            "type_id": "hq",
            "weapon": {
                "staff_of_light": {
                    "name": "Staff of Light",
                    "cost": 18,
                    "uniform": true,
                    "add_on": false,
                    "always_included": true,
                }
            },
            "min": 1,
            "max": 1,
        },
        "deathmark": {
            "id": "deathmark",
            "name": "Deathmark",
            "points": 20,
            "power_points": 5,
            "pp_per_unit": 1,
            "base_weapon_cost": 0,
            "type": "Elite",
            "type_id": "elite",
            "weapon": {
                "synaptic_disintegrator": {
                    "name": "Synaptic Disintegrator",
                    "cost": 0,
                    "uniform": true,
                    "add_on": false,
                    "always_included": true,
                }
            },
            "min": 5,
            "max": 10,
        },
        "destoyer": {
            "id": "destoyer",
            "name": "Destoyer",
            "points": 43,
            "power_points": 3,
            "pp_per_unit": 3,
            "base_weapon_cost": 20,
            "type": "Fast Attack",
            "type_id": "fast_attack",
            "weapon": {
                "gauss_cannon": {
                    "name": "Gauss Cannon",
                    "cost": 20,
                    "uniform": true,
                    "add_on": true,
                    "always_included": true,
                }
            },
            "min": 1,
            "max": 6,
        },
        "destoyer_lord": {
            "id": "destoyer_lord",
            "name": "Destoyer Lord",
            "points": 124,
            "power_points": 8,
            "pp_per_unit": 8,
            "base_weapon_cost": 18,
            "type": "HQ",
            "type_id": "hq",
            "weapon": {
                "staff_of_light": {
                    "name": "Staff of Light",
                    "cost": 18,
                    "uniform": true,
                    "add_on": false,
                    "always_included": false,
                },
                "hyperphase_sword": {
                    "name": "Hyperphase Sword",
                    "cost": 3,
                    "uniform": true,
                    "add_on": false,
                    "always_included": false,
                },
                "voideblade": {
                    "name": "Voideblade",
                    "cost": 6,
                    "uniform": true,
                    "add_on": false,
                    "always_included": false,
                },
                "warscythe": {
                    "name": "Warscythe",
                    "cost": 11,
                    "uniform": true,
                    "add_on": false,
                    "always_included": false,
                },
            },
            "additional_gear": {
                "resurection_orb": {
                    "name": "Resurection Orb",
                    "cost": 35,
                    "uniform": false,
                    "add_on": true,
                    "always_included": false,
                },
                "phylactery": {
                    "name": "Phylactery",
                    "cost": 15,
                    "uniform": false,
                    "add_on": true,
                    "always_included": false,
                },
            },
            "min": 1,
            "max": 6,
        },
        "doom_scythe": {
            "id": "doom_scythe",
            "name": "Doom Scythe",
            "points": 220,
            "power_points": 10,
            "pp_per_unit": 10,
            "base_weapon_cost": 0,
            "type": "Flyer",
            "type_id": "flyer",
            "weapon": {
                "death_ray": {
                    "name": "Death Ray",
                    "cost": 0,
                    "uniform": true,
                    "add_on": false,
                    "always_included": true,
                },
                "tesla_destructor": {
                    "name": "2x Tesla Destructor",
                    "cost": 0,
                    "uniform": true,
                    "add_on": false,
                    "always_included": true,
                }
            },
            "min": 1,
            "max": 1,
        },
        "doomsday_ark": {
            "id": "doomsday_ark",
            "name": "Doomsday Ark",
            "points": 220,
            "power_points": 10,
            "pp_per_unit": 10,
            "base_weapon_cost": 0,
            "type": "Heavy Support",
            "type_id": "heavy_support",
            "weapon": {
                "doomsday_cannon": {
                    "name": "Doomsday Cannon",
                    "cost": 0,
                    "uniform": true,
                    "add_on": false,
                    "always_included": true,
                },
                "gauss_flayer_array": {
                    "name": "2x Gauss Flayer Array",
                    "cost": 0,
                    "uniform": true,
                    "add_on": false,
                    "always_included": true,
                }
            },
            "min": 1,
            "max": 1,
        },
        "flayed_ones": {
            "id": "flayed_ones",
            "name": "Flayed Ones",
            "points": 21,
            "power_points": 5,
            "pp_per_unit": 1,
            "base_weapon_cost": 0,
            "type": "Elite",
            "type_id": "elite",
            "weapon": {
                "flayer_claws": {
                    "name": "Flayer Claws",
                    "cost": 0,
                    "uniform": true,
                    "add_on": false,
                    "always_included": true,
                },
            },
            "min": 5,
            "max": 20,
        },
        "gauss_pylon": {
            "id": "gauss_pylon",
            "name": "Gauss Pylon",
            "points": 475,
            "power_points": 24,
            "pp_per_unit": 24,
            "base_weapon_cost": 0,
            "type": "Fortification",
            "type_id": "fortification",
            "weapon": {
                "gauss_annihilator": {
                    "name": "Gauss Annihilator",
                    "cost": 0,
                    "uniform": true,
                    "add_on": false,
                    "always_included": true,
                },
            },
            "min": 1,
            "max": 1,
        },
        "ghost_ark": {
            "id": "ghost_ark",
            "name": "Ghost Ark",
            "points": 170,
            "power_points": 8,
            "pp_per_unit": 8,
            "base_weapon_cost": 0,
            "type": "Dedicated Transport",
            "type_id": "dedicated_transport",
            "weapon": {
                "gauss_flayer_array": {
                    "name": "2x Gauss Flayer Array",
                    "cost": 0,
                    "uniform": true,
                    "add_on": false,
                    "always_included": true,
                }
            },
            "min": 1,
            "max": 1,
        },
        "heavy_destroyer": {
            "id": "heavy_destroyer",
            "name": "Heavy Destroyer",
            "points": 43,
            "power_points": 4,
            "pp_per_unit": 4,
            "base_weapon_cost": 32,
            "type": "Heavy Support",
            "type_id": "heavy_support",
            "weapon": {
                "heavy_gauss_cannon": {
                    "name": "Heavy Gauss Cannon",
                    "cost": 32,
                    "uniform": true,
                    "add_on": false,
                    "always_included": true,
                }
            },
            "min": 1,
            "max": 3,
        },
        "immortal": {
            "id": "immortal",
            "name": "Immortal",
            "points": 8,
            "power_points": 4,
            "pp_per_unit": (4 / 5),
            "base_weapon_cost": 9,
            "type": "Troop",
            "type_id": "troop",
            "weapon": {
                "gauss_blaster": {
                    "name": "Gauss Blaster",
                    "cost": 9,
                    "uniform": true,
                    "add_on": false,
                    "always_included": false,
                },
                "tesla_carbine": {
                    "name": "Tesla Carbine",
                    "cost": 9,
                    "uniform": true,
                    "add_on": false,
                    "always_included": false,
                }
            },
            "min": 5,
            "max": 10,
        },
        "lord": {
            "id": "lord",
            "name": "Lord",
            "points": 73,
            "power_points": 5,
            "pp_per_unit": 5,
            "base_weapon_cost": 18,
            "type": "HQ",
            "type_id": "hq",
            "weapon": {
                "staff_of_light": {
                    "name": "Staff of Light",
                    "cost": 18,
                    "uniform": true,
                    "add_on": false,
                    "always_included": false,
                },
                "hyperphase_sword": {
                    "name": "Hyperphase Sword",
                    "cost": 3,
                    "uniform": true,
                    "add_on": false,
                    "always_included": false,
                },
                "voideblade": {
                    "name": "Voideblade",
                    "cost": 6,
                    "uniform": true,
                    "add_on": false,
                    "always_included": false,
                },
                "warscythe": {
                    "name": "Warscythe",
                    "cost": 11,
                    "uniform": true,
                    "add_on": false,
                    "always_included": false,
                },
            },
            "additional_gear": {
                "resurection_orb": {
                    "name": "Resurection Orb",
                    "cost": 35,
                    "uniform": true,
                    "add_on": true,
                    "always_included": false,
                },
            },
            "min": 1,
            "max": 1,
        },
        "lynchguard": {
            "id": "lynchguard",
            "name": "Lynchguard",
            "points": 19,
            "power_points": 8,
            "pp_per_unit": (8 / 5),
            "base_weapon_cost": 18,
            "type": "Elite",
            "type_id": "elite",
            "weapon": {
                "hyperphase_sword": {
                    "name": "Hyperphase Sword/Dispersion Shield",
                    "cost": 18,
                    "uniform": true,
                    "add_on": false,
                    "always_included": false,
                },
                "warscythe": {
                    "name": "Warscythe",
                    "cost": 11,
                    "uniform": true,
                    "add_on": false,
                    "always_included": false,
                },
            },
            "min": 5,
            "max": 10,
        },
        "monolith": {
            "id": "monolith",
            "name": "Monolith",
            "points": 381,
            "power_points": 19,
            "pp_per_unit": 19,
            "base_weapon_cost": 0,
            "type": "Heavy Support",
            "type_id": "heavy_support",
            "weapon": {
                "gauss_flayer_arc": {
                    "name": "4x Gauss Flayer Arc",
                    "cost": 0,
                    "uniform": true,
                    "add_on": false,
                    "always_included": true,
                },
                "particle_whip": {
                    "name": "Particle Whip",
                    "cost": 0,
                    "uniform": true,
                    "add_on": false,
                    "always_included": true,
                },
            },
            "min": 1,
            "max": 1,
        },
        "night_shroud": {
            "id": "night_shroud",
            "name": "Night Shroud",
            "points": 270,
            "power_points": 13,
            "pp_per_unit": 13,
            "base_weapon_cost": 0,
            "type": "Flying",
            "type_id": "flying",
            "weapon": {
                "twin_tesla_destructor": {
                    "name": "Twin Tesla Destructor",
                    "cost": 0,
                    "uniform": true,
                    "add_on": false,
                    "always_included": true,
                }
            },
            "min": 1,
            "max": 1,
        },
        "night_scythe": {
            "id": "night_scythe",
            "name": "Night Scythe",
            "points": 174,
            "power_points": 8,
            "pp_per_unit": 8,
            "base_weapon_cost": 0,
            "type": "Flying",
            "type_id": "flying",
            "weapon": {
                "tesla_destructor": {
                    "name": "2x Tesla Destructor",
                    "cost": 0,
                    "uniform": true,
                    "add_on": false,
                    "always_included": true,
                }
            },
            "min": 1,
            "max": 1,
        },
        "obelisk": {
            "id": "obelisk",
            "name": "Obelisk",
            "points": 426,
            "power_points": 21,
            "pp_per_unit": 21,
            "base_weapon_cost": 0,
            "type": "Lord of War",
            "type_id": "lord_of_war",
            "weapon": {
                "tesla_sphere": {
                    "name": "4x Tesla Sphere",
                    "cost": 0,
                    "uniform": true,
                    "add_on": false,
                    "always_included": true,
                },
            },
            "min": 1,
            "max": 1,
        },
        "overlord": {
            "id": "overlord",
            "name": "Overlord",
            "points": 101,
            "power_points": 7,
            "pp_per_unit": 7,
            "base_weapon_cost": 18,
            "type": "HQ",
            "type_id": "hq",
            "weapon": {
                "staff_of_light": {
                    "name": "Staff of Light",
                    "cost": 18,
                    "uniform": true,
                    "add_on": false,
                    "always_included": false,
                },
                "hyperphase_sword": {
                    "name": "Hyperphase Sword",
                    "cost": 3,
                    "uniform": true,
                    "add_on": false,
                    "always_included": false,
                },
                "voideblade": {
                    "name": "Voideblade",
                    "cost": 6,
                    "uniform": true,
                    "add_on": false,
                    "always_included": false,
                },
                "warscythe": {
                    "name": "Warscythe",
                    "cost": 11,
                    "uniform": true,
                    "add_on": false,
                    "always_included": false,
                },
            },
            "additional_gear": {
                "resurection_orb": {
                    "name": "Resurection Orb",
                    "cost": 35,
                    "uniform": false,
                    "add_on": true,
                    "always_included": false,
                },
            },
            "min": 1,
            "max": 1,
        },
        "sentry_pylon": {
            "id": "sentry_pylon",
            "name": "Sentry Pylon",
            "points": 100,
            "power_points": 8,
            "pp_per_unit": 7,
            "base_weapon_cost": 50,
            "type": "Heavy Support",
            "type_id": "heavy_support",
            "weapon": {
                "gauss_exterminator": {
                    "name": "Gauss Exterminator",
                    "cost": 50,
                    "uniform": true,
                    "add_on": false,
                    "always_included": false,
                },
                "focussed_death_ray": {
                    "name": "Focussed Death Ray",
                    "cost": 35,
                    "uniform": true,
                    "add_on": false,
                    "always_included": false,
                },
                "heat_cannon": {
                    "name": "Heat Cannon",
                    "cost": 75,
                    "uniform": true,
                    "add_on": false,
                    "always_included": false,
                },
            },
            "additional_gear": {
                "teleportation_matrix": {
                    "name": "Teleportation Matrix",
                    "cost": 10,
                    "uniform": false,
                    "add_on": true,
                    "always_included": false,
                },
            },
            "min": 1,
            "max": 3,
        },
        "tesseract_ark": {
            "id": "tesseract_ark",
            "name": "Tesseract Ark",
            "points": 220,
            "power_points": 13,
            "pp_per_unit": 13,
            "base_weapon_cost": 40,
            "type": "Heavy Support",
            "type_id": "heavy_support",
            "weapon": {
                "gauss_cannon": {
                    "name": "2x Gauss Cannon",
                    "cost": 40,
                    "uniform": true,
                    "add_on": false,
                    "always_included": false,
                },
                "particle_beamer": {
                    "name": "2x Particle Beamer",
                    "cost": 20,
                    "uniform": true,
                    "add_on": false,
                    "always_included": false,
                },
                "tesla_cannon": {
                    "name": "2x Tesla Cannon",
                    "cost": 26,
                    "uniform": true,
                    "add_on": false,
                    "always_included": false,
                },
                "tesseract_singularity_chamber": {
                    "name": "Tesseract Singulairty Chamber",
                    "cost": 0,
                    "uniform": true,
                    "add_on": false,
                    "always_included": true,
                },
            },
            "min": 1,
            "max": 1,
        },
        "tesseract_vault": {
            "id": "tesseract_vault",
            "name": "Tesseract Vault",
            "points": 496,
            "power_points": 24,
            "pp_per_unit": 24,
            "base_weapon_cost": 0,
            "type": "Lord of War",
            "type_id": "lord_of_war",
            "weapon": {
                "tesla_sphere": {
                    "name": "4x Tesla Sphere",
                    "cost": 0,
                    "uniform": true,
                    "add_on": false,
                    "always_included": true,
                },
            },
            "min": 1,
            "max": 1,
        },
        "tomb_blade": {
            "id": "tomb_blades",
            "name": "Tomb Blade",
            "points": 24,
            "power_points": 5,
            "pp_per_unit": 5,
            "base_weapon_cost": 18,
            "type": "Fast Attack",
            "type_id": "fast_attack",
            "weapon": {
                "gauss_blaster": {
                    "name": "2x Gauss Blaster",
                    "cost": 18,
                    "uniform": true,
                    "add_on": false,
                    "always_included": true,
                },
                "tesla_carbine": {
                    "name": "2x Tesla Carbine",
                    "cost": 18,
                    "uniform": true,
                    "add_on": false,
                    "always_included": true,
                },
                "particle_beamer": {
                    "name": "Particle Beamer",
                    "cost": 10,
                    "uniform": true,
                    "add_on": false,
                    "always_included": true,
                },
            },
            "additional_gear": {
                "shieldvane": {
                    "name": "Shieldvane",
                    "cost": 6,
                    "uniform": false,
                    "add_on": true,
                    "always_included": false,
                },
                "nebluloscope": {
                    "name": "Nebluloscope",
                    "cost": 3,
                    "uniform": false,
                    "add_on": true,
                    "always_included": false,
                },
                "shadowloom": {
                    "name": "Shadowloom",
                    "cost": 5,
                    "uniform": false,
                    "add_on": true,
                    "always_included": false,
                },
            },
            "min": 3,
            "max": 9,
        },
        "tomb_citadel": {
            "id": "tomb_citadel",
            "name": "Tomb Citadel",
            "points": 730,
            "power_points": 40,
            "pp_per_unit": 40,
            "base_weapon_cost": 0,
            "type": "Fortification",
            "type_id": "fortification",
            "weapon": {
                "tesla_destructor": {
                    "name": "Tesla Destructor",
                    "cost": 0,
                    "uniform": false,
                    "add_on": false,
                    "always_included": false,
                },
                "gauss_exterminator": {
                    "name": "Gauss Exterminator",
                    "cost": 50,
                    "uniform": false,
                    "add_on": false,
                    "always_included": false,
                },
            },
            "min": 1,
            "max": 1,
        },
        "transcedent_ctan": {
            "id": "transcedent_ctan",
            "name": "Transcedent C'tan",
            "points": 232,
            "power_points": 12,
            "pp_per_unit": 12,
            "base_weapon_cost": 0,
            "type": "Heavy Support",
            "type_id": "heavy_support",
            "weapon": {
                "crackling_tendrils": {
                    "name": "Crackling Tendrils",
                    "cost": 0,
                    "uniform": true,
                    "add_on": false,
                    "always_included": true,
                },
            },
            "min": 1,
            "max": 1,
        },
        "triarch_praetorian": {
            "id": "triarch_praetorian",
            "name": "Triarch Praetorian",
            "points": 25,
            "power_points": 8,
            "pp_per_unit": 8,
            "base_weapon_cost": 10,
            "type": "Elite",
            "type_id": "elite",
            "weapon": {
                "particle_caster_voidblade": {
                    "name": "Particle Caster/Voidblade",
                    "cost": 10,
                    "uniform": true,
                    "add_on": false,
                    "always_included": false,
                },
                "rod_of_conenant": {
                    "name": "Rod of Covenant",
                    "cost": 10,
                    "uniform": true,
                    "add_on": false,
                    "always_included": false,
                },
            },
            "min": 5,
            "max": 10,
        },
        "triarch_stalker": {
            "id": "triarch_stalker",
            "name": "Triarch Stalker",
            "points": 117,
            "power_points": 8,
            "pp_per_unit": 8,
            "base_weapon_cost": 54,
            "type": "Elite",
            "type_id": "elite",
            "weapon": {
                "heat_ray": {
                    "name": "Heat Ray",
                    "cost": 54,
                    "uniform": true,
                    "add_on": false,
                    "always_included": false,
                },
                "particle_shredder": {
                    "name": "Particle Shredder",
                    "cost": 41,
                    "uniform": true,
                    "add_on": false,
                    "always_included": false,
                },
                "twin_heavy_gauss_cannon": {
                    "name": "Twin Heavy Gauss Cannon",
                    "cost": 64,
                    "uniform": true,
                    "add_on": false,
                    "always_included": false,
                },
                "massive_forelimbs": {
                    "name": "Massive Forelimbs",
                    "cost": 0,
                    "uniform": true,
                    "add_on": false,
                    "always_included": true,
                },
            },
            "min": 1,
            "max": 1,
        },
        "warrior": {
            "id": "warrior",
            "name": "Warrior",
            "points": 12,
            "power_points": 10,
            "pp_per_unit": (6 / 10),
            "base_weapon_cost": 0,
            "type": "Troop",
            "type_id": "troop",
            "weapon": {
                "gauss_flayer": {
                    "name": "Gauss Flayer",
                    "cost": 0,
                    "uniform": true,
                    "add_on": false,
                    "always_included": true,
                },
            },
            "min": 10,
            "max": 20,
        },
    }

    var intRow = 0;

    //  initialize the local storage
    localStorage.setItem('elite', 0);
    localStorage.setItem('dedicated_transport', 0);
    localStorage.setItem('fast_attack', 0);
    localStorage.setItem('flyer', 0);
    localStorage.setItem('fortification', 0);
    localStorage.setItem('heavy_support', 0);
    localStorage.setItem('hq', 0);
    localStorage.setItem('lord_of_war', 0);
    localStorage.setItem('troop', 0);

    //  when adding a new unit
    $('form#add_new').submit(function (e) {
        e.preventDefault();
        var form = this;
        var objUnit = arrayUnit[$("#unit_select").val()];

        var strWeaponSelectId = "weapon_select_" + intRow;
        var strGearSelectId = "gear_select_" + intRow;

        var intBaseUnitTotal = parseInt(parseInt(objUnit.points) * parseInt(objUnit.min)) + parseInt(parseInt(objUnit.min) * parseInt(objUnit.base_weapon_cost));

        $("#army_list").append("<div><form class='form_unit' id='row_" + intRow + "'><div>" + objUnit.name + "</div><div>" + objUnit.points + "</div><div><input name='models_per_unit' class='models_per_unit' type='number' value='" + objUnit.min + "' min='" + objUnit.min + "'  max='" + objUnit.max + "'/></div><div>" + objUnit.type + "<input type='hidden' name='unit_type' value='" + objUnit.type_id + "' /></div><div><ul class='weapon_select' id='" + strWeaponSelectId + "'></ul></div><div>Additional Gear<ul class='gear_select' id='" + strGearSelectId + "'></ul></div><div><button class='remove_unit'>Remove</button></div><div class='div_total'>" + intBaseUnitTotal + "</div><input type='hidden' class='base_point' value='" + objUnit.points + "' name='base_point' /></form></div>");

        localStorage[objUnit.type_id]++;    //  add one to the local storage

        //  backass way to get the count of the weapon array in the object
        var countWeapon = 0;
        for (var weapon in objUnit.weapon) {
            if (objUnit.weapon.hasOwnProperty(weapon)) {
                countWeapon++;
            }
        }
        var countGear = 0;
        for (var additional_gear in objUnit.additional_gear) {
            if (objUnit.additional_gear.hasOwnProperty(additional_gear)) {
                countGear++;
            }
        }

        //  if there is only weapon, just list the weapon and count
        if (countWeapon == 1) {
            var name = "";
            $.each(objUnit.weapon, function (i, val) {
                name = val.name;
            });
            strWeaponSelect = "<li><span class='weapon_total'>" + objUnit.min + "</span>x " + name + "</li>";
        } else {
            //  else foreach weapon add a new LI
            strWeaponSelect = "";
            $.each(objUnit.weapon, function (i, val) {
                strWeaponSelect += "<li id='" + i + "'></li>";
            });
        }

        $("#" + strWeaponSelectId).append(strWeaponSelect); //  append all the li to the ul.weapon_select

        //  if there is only add gear, just list the gear and count
        if (countWeapon == 1) {
            var name = "";
            $.each(objUnit.additional_gear, function (i, val) {
                name = val.name;
            });
            strGearSelect = "<li><span class='weapon_total'>" + objUnit.min + "</span>x " + name + "</li>";
        } else {
            //  else foreach weapon add a new LI
            strGearSelect = "";
            $.each(objUnit.additional_gear, function (i, val) {
                strGearSelect += "<li id='" + i + "'></li>";
            });
        }

        $("#" + strGearSelectId).append(strGearSelect); //  append all the li to the ul.weapon_select

        //  if there are multiple weapons, go through and generate the selection
        if (countWeapon > 1) {
            var j = 0;
            var intWeaponCount = 0;
            //  for each weapon
            $.each(objUnit.weapon, function (i, val) {
                intWeaponCount = 0;
                strWeaponSelect = "";
                //  if it is the first weapon, set it as the default
                if (j == 0) {
                    intWeaponCount = objUnit.min;
                }

                //  if the unit has to have all the same weapon selection
                if (val.uniform == true) {
                    //  if the weapon is always include
                    if (val.always_included == true) {
                        //  if the weapon is a add_on
                        if (val.add_on == true) {

                        } else {
                            strWeaponSelect += "<span class='weapon_total'>" + objUnit.min + "</span>x " + val.name;
                            strWeaponSelect += "<input type='hidden' value='" + intWeaponCount + "' name='weapon_input' />";
                        }
                    } else {
                        //  if the weapon is a add_on
                        if (val.add_on == true) {
                            strWeaponSelect += "<span class='weapon_total'>" + objUnit.min + "</span>x " + val.name;
                            strWeaponSelect += "<input type='hidden' value='" + intWeaponCount + "' name='weapon_input' />";
                        } else {
                            strWeaponSelect += "<input type='radio' name='weapon_select_uniform' value='" + val.cost + "'";
                            //  if it is the first weapon, make it the default and checked
                            if (j == 0) {
                                strWeaponSelect += " checked='checked'";
                            }
                            strWeaponSelect += " />";
                            strWeaponSelect += "<span class='weapon_total'>" + objUnit.min + "</span>x " + val.name;
                            strWeaponSelect += "<input type='hidden' value='" + intWeaponCount + "' name='weapon_input' />";
                        }
                    }
                } else {
                    //  if the weapon is always include
                    if (val.always_included == true) {
                        //  if the weapon is a add_on
                        if (val.add_on == true) {

                        } else {

                        }
                    } else {
                        //  if the weapon is a add_on
                        if (val.add_on == true) {
                            strWeaponSelect += "<label>" + val.name + "<input name='weapon_count' type='number' min='0' max='" + objUnit.min + "' value='" + intWeaponCount + "' class='weapon_count' id='" + i + "_input' /><input type='hidden' value='" + val.cost + "' name='weapon_cost' /></label>";
                        } else {

                        }
                    }
                }

                //strWeaponSelect += "<input type='hidden' value='" + val.cost + "' name='weapon_cost' />";
                j++;
                $("#" + strWeaponSelectId + " #" + i).append(strWeaponSelect);  //  finally append the weapon selection to the element
            });
        }

        //  if there is only gear, just list the weapon and count
        if (countGear >= 1) {
            var name = "";
            // $.each(objUnit.additional_gear, function (i, val) {
            //     name = val.name;
            // });
            // strGearSelect = "<li><span class='gear_total'>" + objUnit.min + "</span>x " + name + "</li>";
            $.each(objUnit.additional_gear, function (i, val) {
                intGearCount = 0;
                strGearSelect = "";
                //  if it is the first weapon, set it as the default
                if (j == 0) {
                    intGearCount = objUnit.min;
                }

                //  if the unit has to have all the same weapon selection
                if (val.uniform == true) {
                    //  if the weapon is always include
                    if (val.always_included == true) {
                        //  if the weapon is a add_on
                        if (val.add_on == true) {

                        } else {
                            strGearSelect += "<span class='weapon_total'>" + objUnit.min + "</span>x " + val.name;
                            strGearSelect += "<input type='hidden' value='" + intGearCount + "' name='weapon_input' />";
                        }
                    } else {
                        //  if the weapon is a add_on
                        if (val.add_on == true) {
                            strGearSelect += "<span class='weapon_total'>" + objUnit.min + "</span>x " + val.name;
                            strGearSelect += "<input type='hidden' value='" + intGearCount + "' name='weapon_input' />";
                        } else {
                            strGearSelect += "<input type='radio' name='weapon_select_uniform' value='" + val.cost + "'";
                            //  if it is the first weapon, make it the default and checked
                            if (j == 0) {
                                strGearSelect += " checked='checked'";
                            }
                            strGearSelect += " />";
                            strGearSelect += "<span class='weapon_total'>" + objUnit.min + "</span>x " + val.name;
                            strGearSelect += "<input type='hidden' value='" + intGearCount + "' name='weapon_input' />";
                        }
                    }
                } else {
                    //  if the weapon is always include
                    if (val.always_included == true) {
                        //  if the weapon is a add_on
                        if (val.add_on == true) {

                        } else {

                        }
                    } else {
                        //  if the weapon is a add_on
                        if (val.add_on == true) {
                            strGearSelect += "<label>" + val.name + "<input name='gear_count' type='number' min='0' max='" + objUnit.min + "' value='" + intGearCount + "' class='gear_count' id='" + i + "_input' /><input type='hidden' value='" + val.cost + "' name='gear_cost' /></label>";
                        } else {

                        }
                    }
                }

                //strWeaponSelect += "<input type='hidden' value='" + val.cost + "' name='weapon_cost' />";
                j++;
                $("#" + strGearSelectId + " #" + i).append(strGearSelect);  //  finally append the weapon selection to the element
            });
        }

        intRow++;
        recalculate_army(); //  run the total army calculation
    });

    //  when the remove button is clicked
    $("#army_list").on('click', '.remove_unit', function (e) {
        e.preventDefault(); //  don't need to submit here

        var form = $(this).parents("div form.form_unit");
        var strType = $("input[name='unit_type']", form).val();

        form.remove(); //  remove the whole div

        localStorage[strType]--;    //  remove the one from the local storage

        recalculate_army(); //  run the total army calculation
    })

    //  when updating the models per unit
    $("#army_list").on('click', '.models_per_unit', function () {
        var form = $(this).parents('form.form_unit');

        console.dir(form);

        var intPointsPerUnit = $("input[name='models_per_unit']", form).val() * $("input[name='base_point']", form).val();  //  total cost of the unit
        //  TODO add costs of weapon choice and additional gear choice

        var intTotal = intPointsPerUnit;

        $(".weapon_total", form).text($("input[name='models_per_unit']", form).val());
        //$(".weapon_count", form).attr({ "value": $("input[name='models_per_unit']", form).val() });
        if ($(".weapon_count", form).attr('max')) {
            $(".weapon_count", form).attr({ "max": $("input[name='models_per_unit']", form).val() });
        }
        $(".gear_total", form).text($("input[name='models_per_unit']", form).val());
        //$(".weapon_count", form).attr({ "value": $("input[name='models_per_unit']", form).val() });
        if ($(".gear_count", form).attr('max')) {
            $(".gear_count", form).attr({ "max": $("input[name='models_per_unit']", form).val() });
        }

        recalculate_total(form);
    });

    //  when the radio selection changes
    $("#army_list").on('change', 'input[name=weapon_select_uniform]:radio', function () {
        var form = $(this).parents('form.form_unit');   //  get the parent form
        var strInputSelector = this.value + "_input";   //  get the id of the radio button

        //  switch around the radio buttons and what is selected
        $(".weapon_count", form).attr("disabled", true);
        $(".weapon_count", form).val(0);
        $("#" + strInputSelector, form).attr("disabled", false);
        $("#" + strInputSelector, form).val($("input[name='models_per_unit']", form).val());
        recalculate_total(form);    //  run the unit recalculation
    });

    //  when the weapon amount changes
    $("#army_list").on('change', '.weapon_count', function () {
        var form = $(this).parents('form.form_unit');   //  get the parent form
        recalculate_total(form);    //  run the unit recalculation
    });

    //  when the radio selection changes
    $("#army_list").on('change', 'input[name=gear_select_uniform]:radio', function () {
        var form = $(this).parents('form.form_unit');   //  get the parent form
        var strInputSelector = this.value + "_input";   //  get the id of the radio button

        //  switch around the radio buttons and what is selected
        $("#" + strInputSelector, form).val($("input[name='models_per_unit']", form).val());
        recalculate_total(form);    //  run the unit recalculation
    });

    //  when the gear amount changes
    $("#army_list").on('change', '.gear_count', function () {
        var form = $(this).parents('form.form_unit');   //  get the parent form
        recalculate_total(form);    //  run the unit recalculation
    });

    //  loads the array of units to the select, should be in a DB
    $.each(arrayUnit, function (i, val) {
        $("#unit_select").append("<option value='" + val.id + "'>" + val.name + "</option>");
    });

    function recalculate_total(objForm) {
        var intWeaponCost = 0;

        //  if there is a selected weapon with a cost, calculate the weapon cost
        if ($("input[name='weapon_select_uniform']:checked", objForm).length) {
            intWeaponCost += $("input[name='weapon_select_uniform']:checked", objForm).val() * $("input[name='models_per_unit']", objForm).val();
        }

        //  if there are more then one weapon, loop through each and calculate the total
        if ($(".weapon_select li", objForm).length) {
            $(".weapon_select li", objForm).each(function (li) {
                //  if there is a value, exludes the automatic included weapons
                if ($(this).find('input[name=weapon_cost]').val() >= 0) {
                    intWeaponCost += $(this).find('input[name=weapon_cost]').val() * $(this).find('input[name=weapon_count]').val();
                }
            });
        }

        //  if there is a selected weapon with a cost, calculate the weapon cost
        if ($("input[name='gear_select_uniform']:checked", objForm).length) {
            intWeaponCost += $("input[name='gear_select_uniform']:checked", objForm).val() * $("input[name='models_per_unit']", objForm).val();
        }

        //  if there are multiple additional gear to analyze
        if ($(".gear_select li", objForm).length) {
            $(".gear_select li", objForm).each(function (li) {
                //  if there is a value, exludes the automatic included gear
                if ($(this).find('input[name=gear_cost]').val() >= 0) {
                    intWeaponCost += $(this).find('input[name=gear_cost]').val() * $(this).find('input[name=gear_count]').val();
                }
            });
        }

        var intTotal = intWeaponCost + $("input[name='models_per_unit']", objForm).val() * $("input[name='base_point']", objForm).val();    //  add the weapon cost to the unit cost
        $(".div_total", objForm).text(intTotal);   //  display the unit total cost

        recalculate_army(); //  run the total army calculation
    }

    function recalculate_army() {
        var intArmyTotal = 0;
        //  sum each unit total
        $(".div_total").each(function () {
            intArmyTotal += parseInt(this.textContent);
        });
        $("#army_list_total").text(intArmyTotal);
    }
});